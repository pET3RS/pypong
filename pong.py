
import pygame
from pygame.locals import (
    K_ESCAPE,
    K_SPACE,
    K_w,
    K_s,
    K_j,
    K_k,
    KEYDOWN,
    QUIT,
)
import random

# Default values
BG_COLOR = "black"
FG_COLOR = "white"

SCREEN_WIDTH = 800
SCREEN_HEIGTH = 600
BORDER_MARGIN = 4

BALL_SPEED = 3
BALL_R = 11

PAD_SPEED = 4
PAD_SIZE = 100
PAD_WIDTH_SCALE = 0.1

PLAYER0 = 0
PLAYER1 = 1


'''
|-------------------|-------------------|
|     direction     |     bounce        |
|-------------------|-------------------|
| x  y | meaning    | x  y | meaning    |
|------|------------|------|------------|
|-1 -1 | down+left  |-1  1 | up+left    | bottom, left
|-1  1 | up+left    |-1 -1 | down+left  | top, left
| 1 -1 | right+down | 1  1 | up+right   | bottom, right
| 1  1 | up+right   | 1 -1 | down+right | top, right
|------|------------|------|------------|
'''

class Ball:

    def __init__(self, screen, position, radius, speed=BALL_SPEED, color=FG_COLOR):
        """
        Draws the ball at specified position on the specified screen and sets
        its speed and color.

        screen   - pygame display
        position - tuple containing two integers representing the position of the ball
        radius   - integer representing the radius of the ball
        color    - string containing hex color of the ball
        """

        self._position = position
        self._vector = random.choice([(-1,-1), (-1, 1), (1, -1), (1, 1)])
        self._radius = radius
        self._color = color
        self._screen = screen
        self._speed = speed
        self._hitbox = pygame.draw.circle(screen, color, position, radius)

    def move(self, pad_hitboxes=None):
        """
        Moves the ball. If the pad_hitboxes aren't defined the ball ignores them.

        pad_hitboxes - a list of hitboxes from pygame library

        returns - number of the player who scored (0 or 1) or None if noone sored
        """

        scored = None

        # Collision check
        if pad_hitboxes is not None:
            pad_collision = self._hitbox.collidelist(pad_hitboxes)
            if pad_collision != -1: #-1 means no colisions
                self._vector = (-1*self._vector[0], self._vector[1])
                self._position = (self._position[0]+self._vector[0]*self._speed, self._position[1]+self._vector[1]*self._speed)
                self._hitbox = pygame.draw.circle(self._screen, self._color, self._position, self._radius)
                return scored

        # Calculate new position
        self._position = (self._position[0]+self._vector[0]*self._speed, self._position[1]+self._vector[1]*self._speed)

        # aliases for clarity
        x = self._position[0]
        y = self._position[1]
        margin = BORDER_MARGIN+self._radius

        # Passing the left border
        if x <= margin:
            if (y <= margin and y >= SCREEN_HEIGTH-margin):
                self._vector = (-1*self._vector[0], -1*self._vector[1])
            else:
                self._vector = (-1*self._vector[0], self._vector[1])
            scored = PLAYER1

        # Passing the right border
        elif x >= SCREEN_WIDTH-margin:
            if (y <= margin and y >= SCREEN_HEIGTH-margin):
                self._vector = (-1*self._vector[0], -1*self._vector[1])
            else:
                self._vector = (-1*self._vector[0], self._vector[1])
            scored = PLAYER0

        # Passing the top or bottom border
        elif (y <= margin or y >= SCREEN_HEIGTH-margin):
            if (x <= margin and x >= SCREEN_WIDTH-margin):
                self._vector = (-1*self._vector[0], -1*self._vector[1])
            else:
                self._vector = (self._vector[0], -1*self._vector[1])

        # Redraw the ball on new position
        self._hitbox = pygame.draw.circle(self._screen, self._color, self._position, self._radius)

        return scored

    def reset_position(self, player_scored):
        """
        Resets the position of the ball and sets the initial dirrection for the ball.

        player_scored - number of the player who scored (0 or 1) to determine
                        next dirrection of the ball (the ball vector is set
                        to the side of the player who scored)
        """
        self._position = (SCREEN_WIDTH//2-self._radius, SCREEN_HEIGTH//2-self._radius)
        if player_scored == PLAYER0:
            self._vector = random.choice([(-1, -1),(-1, 1)])
        else:
            self._vector = random.choice([(1, -1), (1, 1)])

    def get_hitbox(self):
        """
        Reteurns pygame hitbox of the ball.
        """
        return self._hitbox



class Pad:

    def __init__(self, screen, position, size, speed, color=FG_COLOR):

        """
        screen   - pygame display
        position - tuple containing 2 numbers representing x and y position
        size     - a number that determines how large is the pad
                   (the height of rectangle) and is used to for scaled width
        speed    - a number that determines how fast will the padd move
                   (number of pixels to move on button press)
        color    - a hex representation of color of the pad
        """

        self._screen = screen
        self._position = position
        self._speed = speed
        self._color = color
        self._width = PAD_WIDTH_SCALE*size
        self._height = size
        self._hitbox = None

    def draw(self):
        """
        Draws the pad based on the pad attributes.
        """
        self._hitbox = pygame.draw.rect(self._screen,
                         self._color,
                         (self._position[0], self._position[1],
                          self._width, self._height))

    def move_up(self):
        """
        Moves the pad upwards based on the speed of the pad.
        """
        # Alter the position
        self._position = (self._position[0], self._position[1]-self._speed)


        if self._position[1] <= BORDER_MARGIN:
            self._position = (self._position[0], BORDER_MARGIN)

        # Redraw
        self.draw()


    def move_down(self):
        """
        Moves the pad downwards based on the speed of the pad.
        """

        # Alter the position
        self._position = (self._position[0], self._position[1]+self._speed)

        if self._position[1]+self._height >= SCREEN_HEIGTH-BORDER_MARGIN:
            self._position = (self._position[0], SCREEN_HEIGTH-self._height-BORDER_MARGIN)

        # Redraw
        self.draw()


    def get_speed(self):
        return self._speed

    def set_speed(self, new_speed):
        self._speed = new_speed

    def set_size(self, new_size):
        self._height = new_size

    def get_hitbox(self):
        return self._hitbox


pygame.init()

clock = pygame.time.Clock()
font = pygame.font.Font("square_pixel-7.ttf", 30)
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGTH))


frame = 0
score = [0, 0]
def update_score(player):
    score[player] += 1

# Menu
in_menu = True
pong = False


## Text information
text = font.render('Press SPACE to play', False, FG_COLOR)
text_rectangle = text.get_rect(center=(SCREEN_WIDTH//2, SCREEN_HEIGTH//2))
display_text = True

## Init the ball
menu_ball1 = Ball(screen, (400, 300), 10, speed=4, color=FG_COLOR)
menu_ball2 = Ball(screen, (400, 300), 10, speed=6, color=FG_COLOR)

while in_menu:

    # Background
    screen.fill(BG_COLOR)

    # Border
    pygame.draw.rect(screen,
                     FG_COLOR,
                     (BORDER_MARGIN, BORDER_MARGIN,
                      SCREEN_WIDTH-BORDER_MARGIN*2,
                      SCREEN_HEIGTH-BORDER_MARGIN*2),
                     1, 1)

    # Blinking text
    if frame % 60 == 0:
        display_text = False

    if frame % 120 == 0:
        display_text = True

    if display_text == True:
        screen.blit(text, text_rectangle)

    # Moving balls
    menu_ball1.move()
    menu_ball2.move()

    # Update the screen
    clock.tick(60)
    pygame.display.flip()
    frame+=1

    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_SPACE:
                pong = True
                in_menu = False
                break
            if event.key == K_ESCAPE:
                pong = False
                in_menu = False
                break

        if event.type == QUIT:
            pong = False
            in_menu = False
            break


# Pong

ball = Ball(screen, (400, 300), BALL_R, speed=BALL_SPEED, color="red")
pad0 = Pad(screen, (BORDER_MARGIN, SCREEN_HEIGTH//2-PAD_SIZE//2), PAD_SIZE, PAD_SPEED, FG_COLOR)
pad1 = Pad(screen, (SCREEN_WIDTH-BORDER_MARGIN-PAD_SIZE*0.1, SCREEN_HEIGTH//2-PAD_SIZE//2), PAD_SIZE, PAD_SPEED, FG_COLOR)
font = pygame.font.Font("square_pixel-7.ttf", 20)

while pong:

    # Background
    screen.fill(BG_COLOR)

    # Border
    pygame.draw.rect(screen,
                     FG_COLOR,
                     (BORDER_MARGIN, BORDER_MARGIN,
                      SCREEN_WIDTH-BORDER_MARGIN*2,
                      SCREEN_HEIGTH-BORDER_MARGIN*2),
                     1, 1)

    # Score
    text = font.render(f'Player0: {score[0]} | Player1: {score[1]}', False, FG_COLOR)
    text_rectangle = text.get_rect(center=(SCREEN_WIDTH//2, 20))
    screen.blit(text, text_rectangle)

    # Pads movement
    keys = pygame.key.get_pressed()

    if keys[K_w]:
        pad0.move_up()
    if keys[K_s]:
        pad0.move_down()
    if keys[K_j]:
        pad1.move_down()
    if keys[K_k]:
        pad1.move_up()

    # Draw the pads
    pad0.draw()
    pad1.draw()

    # Ball movement
    pad_hitboxes = [pad0.get_hitbox(), pad1.get_hitbox()]
    player_scored = ball.move(pad_hitboxes)

    # If a player scores
    if player_scored is not None:
        update_score(player_scored)
        ball.reset_position(player_scored)

    # Update the screen
    clock.tick(60)
    pygame.display.flip()

    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                pong = False

        elif event.type == QUIT:
            pong = False

